<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title><?php echo env('APP_NAME') . ' | ' . $page; if(env('APP_ENV') == "local"){ ?> - Local <?php }else if(env('APP_ENV') == "beta"){ ?> Beta <?php } ?> </title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" />
    <link rel="stylesheet" href="{{ asset("css/main.css") }}" />
</head>
<body>

    <div class="website">
        <div class="website-inner">
            <div class="website-header">
                @include('logged.header')
            </div>
            <div class="website-content">

            </div>
            <div class="website-footer">
                @include('logged.footer')
            </div>
        </div>
    </div>

<!-- Logged JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script>
var env = "<?php echo env('APP_ENV'); ?>";

if(env != "production")
{
    Pusher.logToConsole = true;
}

// Connect
var pusher = new Pusher("<?php echo env('PUSHER_APP_KEY'); ?>", {
    cluster: 'us2',
    encrypted: true
});

// Connect logged user to his specific channel

</script>
</body>
</html>