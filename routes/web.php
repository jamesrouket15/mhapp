<?php

use Illuminate\Support\Facades\Route;

/* Controllers */
use App\Http\Controllers\TimelineController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Timeline Routes */
Route::get('/dashboard', [DashboardController::class, 'index'])->middleware(['auth'])->name('dashboard');;

/* Tests */
Route::get('/tests/pusher_test', function () {
    return view('test_grounds/pusher_test');
});

Route::get('/tests/diary_functions', function () {
    return view('test_grounds/diary_functions');
});

require __DIR__.'/auth.php';
