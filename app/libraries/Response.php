<?php
namespace App\Libraries;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class Response
{
    public $code = "";
    public $status = "";

    public function __construct()
    {
        
    }

    /**
     * Send
     * ------
     * This function will actually get the information of the response, then encrypt it, then send it.
     * 
     * @var $code (Int);
     * @var $body (Text);
     * @var $json (Boolean);
     * 
     */
    public function Send($code, $body, $json = true, $payload = "")
    {
        if(!empty($code) && !empty($body))
        {
            // Array
            $response = array(
                'code' => $code,
                'body' => $body,
                'payload' => $payload
            );

            // Json
            if($json == true)
            {
                $json = json_encode($response);

                // Encrypt
                $encrypted = $this->EncryptResponse($json);

                // Create response
                return json_encode(
                    array(
                        'r_timestamp' => Date("d-m-y H:i:s"),
                        'r_body' => $encrypted,
                        'r_encrypted' => true
                    )
                );
            }
        }
        
        return false;
    }

    /**
     * DecryptResponse
     * ------
     * This function will decrypt the response 
     * 
     * @var $body (Text)
     * 
     */
    public function DecryptResponse($body)
    {
        if(!empty($body))
        {
            return Crypt::decryptString($body);
        }

        return false;
    }

    /**
     * EncryptResponse
     * ------
     * This will encrypt the response
     * 
     * @var $body (Text)
     * 
     */
    public function EncryptResponse($body)
    {
        if(!empty($body))
        {
            return Crypt::encryptString($body);
        }

        return false;
    }

    /** 
     * Unpack
     * ------
     * This will unpack the response. First the reponse will be unencrypted, then the text will turn from JSON into a PHP array
     * 
     * @var $response (Text)
     */
    public function Unpack($encrypted)
    {
        if(!empty($encrypted))
        {
            // Decode
            $decode = json_decode($encrypted);

            if($decode->r_encrypted == true)
            {
                return $this->DecryptResponse($decode->r_body);
            }
        }

        return false;
    }

    /**
     * Read
     * ------
     * This will allow the user to read the actual response
     */
    public function Read($response)
    {
        if(!empty($response))
        {
            // Lets unpack the response
            $unpacked = $this->Unpack($response);

            // Now send it
            return json_decode($unpacked);
        }

        return false;
    }
}