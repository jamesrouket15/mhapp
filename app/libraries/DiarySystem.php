<?php
namespace App\Libraries;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

use App\Libraries\Response;
use Illuminate\Auth\EloquentUserProvider;

class DiarySystem
{
    public $response;

    public function __construct()
    {
        // Initiate
        $this->response = new Response();
    }

    /**
     * DoesEntryExists
     * ------
     * This will search the database for the entry and returns either a 0 or 1
     * for a response
     * 
     * $eid = ID of the entry
     */
    public function DoesEntryExist($eid)
    {
        if(!empty($eid))
        {
            // Lets run the query
            return DB::select("SELECT id FROM " . env('DB_PREFIX') . "Diary_Entries WHERE unique_id=?", [''.$eid.'']);
        }
    }

    /**
     * DoesDiaryExists
     * ------
     * This will search the database for the diary and return either a 0 or 1
     * for a response
     * 
     * $did = ID of the diary
     */
    public function DoesDiaryExist($did)
    {
        if(!empty($did))
        {
            // Lets run the query
            return DB::select("SELECT id FROM " . env('DB_PREFIX') . "Diary WHERE unique_id=?", [''.$did.'']);
        }
    }

    /**
     * CreateDiary
     * ------
     * This will create the diary for us. Diaries will hold all the entries
     * 
     * $title = Title of the entry
     * $description = Description of the diary
     * $privacy = Privacy of the diary
     * $uid = ID of the person thats making it
     */
    public function CreateDiary($title, $description, $privacy, $uid)
    {
        if(!empty($title) 
        && !empty($description) 
        && !empty($privacy) 
        && !empty($uid))
        {
            // Lets create an id
            $str = md5(Crypt::encrypt("diary_" . Str::random(65) . rand(0, 10000)));

            // Lets make sure the title is less than 35 charactors long
            if(Str::length($title) > 35)
            {
                return $this->response->send(500, 'Your title must be under 35 characters');
            }

            // Make sure the description is less than 140 charactors long
            if(Str::length($description) > 140)
            {
                return $this->response->send(500, 'Your description must be under 140 characters');
            }

            // Okay our checks are good lets insert
            if(DB::table('Diary')->insert([
                'unique_id' => $str,
                'user_id' => $uid,
                'diary_name' => $title,
                'diary_description' => $description,
                'diary_privacy' => $privacy,
                'diary_deleted' => '0',
                'diary_created' => date("Y-m-d H:i:s"),
            ]))
            {
                // Now we can send it back
                return $this->response->send(200, 'Your diary has been created', true, array('did' => $str));
            }

        } else{
            return $this->response->send(500, 'Please enter your title and description!');
        }

        return $this->response->send(500, 'Error has occured, please refresh the page!');
    }

    /**
     * CreateDiaryEntry
     * ------
     * 
     * 
     */
    public function CreateDiaryEntry($did, $title = "Entry ", $body, $privacy = "0")
    {

    }

    /**
     * UpdateDiaryLastEntry
     * ------
     * This function will update the diary of its last entry
     * 
     * $did = ID of the diary 
     * $timestamp = Timestamp of the change
     */
    public function UpdateDiaryLastEntry($did, $timestamp)
    {
        if(!empty($eid) && !empty($timestamp))
        {
            // Make sure the entry exists
            if(count($this->DoesDiaryExist($did)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary SET diary_last_entry_time=? WHERE unique_id=?", [''.$timestamp.'', ''.$did.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * UpdateDiaryEntry
     * ------
     * This function will update the info of the diary
     * 
     * $did = ID of the diary 
     * $timestamp = Timestamp of the change
     */
    public function UpdateDiaryInfo($did, $timestamp)
    {
        if(!empty($did))
        {
            // Make sure the entry exists
            if(count($this->DoesDiaryExist($did)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary SET diary_last_entry_time=? WHERE unique_id=?", [''.$timestamp.'', ''.$did.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * UpdateDiaryPrivacy
     * ------
     * This function will update the privacy of the diary
     * 
     * $did = ID of the diary 
     * $privacy = Privacy type
     * $timestamp = Timestamp of the change
     */
    public function UpdateDiaryPrivacy($did, $privacy, $timestamp)
    {
        if(!empty($eid) && !empty($privacy))
        {
            // Make sure the entry exists
            if(count($this->DoesDiaryExist($did)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary SET diary_privacy=? WHERE unique_id=?", [''.$privacy.'', ''.$did.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return $this->_response->send(200, 'Diary privacy has been updated successfully.');
                }
            }
        }

        return false;
    }

    public function CreateDiarySharableLink($did)
    {
        if(!empty($did))
        {
            // Make sure the entry exists
            if(count($this->DoesDiaryExist($did)) == 1)
            {
                // Link blob
                $str = Str::random(12);

                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary SET diary_shareable_link=? WHERE unique_id=?", [''.$str.'', ''.$did.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return true;
                }
            }
        }

        return false;
    }

    public function CreateDiaryEntryShareableLink($eid)
    {
        if(!empty($eid))
        {
            // Make sure the entry exists
            if(count($this->DoesDiaryExist($eid)) == 1)
            {
                // Link blob
                $str = Str::random(12);

                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary_entry SET entry_shareable_link=? WHERE unique_id=?", [''.$str.'', ''.$eid.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return true;
                }
            }
        }

        return false;
    }

    public function DeleteDiary($did)
    {
        if(!empty($did))
        {
            // Make sure the entry exists
            if(count($this->DoesDiaryExist($did)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary SET diary_deleted=1 WHERE unique_id=?", [''.$did.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return $this->_response->send(200, 'Diary has been deleted successfully.');
                }
            }
        }

        return $this->_response->send(500, 'Error has occured, please refresh the page!');
    }

    public function DeleteDiaryEntry($eid)
    {
        if(!empty($eid))
        {
            // Make sure the entry exists
            if(count($this->DoesEntryExist($eid)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary_Entries SET entry_deleted=1 WHERE unique_id=?", [''.$eid.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return $this->_response->send(200, 'Entry has been deleted successfully.');
                }
            }
        }

        return $this->_response->send(500, 'Error has occured, please refresh the page!');
    }

    public function UpdateDiaryEntryPrivacy($eid, $privacy)
    {
        if(!empty($eid) && !empty($privacy))
        {
            // Make sure the entry exists
            if(count($this->DoesEntryExist($eid)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary_Entries SET entry_privacy=? WHERE unique_id=?", [''.$privacy.'', ''.$eid.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return $this->_response->send(200, 'Entry has been updated successfully.');
                }
            }
        }

        return $this->_response->send(500, 'Error has occured, please refresh the page!');
    }

    public function UpdateDiaryEntryText($eid, $text)
    {
        if(!empty($eid) && !empty($text))
        {
            // Make sure the entry exists
            if(count($this->DoesEntryExist($eid)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary_Entries SET entry_body=? WHERE unique_id=?", [''.$text.'', ''.$eid.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return $this->_response->send(200, 'Entry text has been updated successfully.');
                }
            }
        }

        return $this->_response->send(500, 'Error has occured, please refresh the page!');
    }

    public function UpdateDiaryEntryTitle($eid, $title)
    {
        if(!empty($eid) && !empty($title))
        {
            // Make sure the entry exists
            if(count($this->DoesEntryExist($eid)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary_Entries SET entry_text=? WHERE unique_id=?", [''.$title.'', ''.$eid.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return $this->_response->send(200, 'Entry title has been updated successfully.');
                }
            }
        }

        return $this->_response->send(500, 'Error has occured, please refresh the page!');
    }

    public function UpdateDiaryEntryLastUpdate($eid, $timestamp)
    {
        if(!empty($eid) && !empty($timestamp))
        {
            // Make sure the entry exists
            if(count($this->DoesEntryExist($eid)) == 1)
            {
                // Update
                $update = DB::update("UPDATE " . env('DB_PREFIX') . "Diary_Entries SET entry_last_update=? WHERE unique_id=?", [''.$timestamp.'', ''.$eid.'']);

                // Lets check
                if($update == 1)
                {
                    // We're good
                    return true;
                }
            }
        }

        return false;
    }

    public function UpdateEntry()
    {
        
    }
}